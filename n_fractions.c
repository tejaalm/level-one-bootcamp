//WAP to find the sum of n fractions.
#include<stdio.h>

typedef struct
{
 int num;
 int den;

}fract;

typedef struct
{
    fract fr[100],sum;
    int n;
 }frc;


fract input()
{
   fract retval; 
    printf("enter the numerator and denominator \n");
    scanf("%d %d",&retval.num,&retval.den);
    return retval;
}
void input_n(frc *f)
{  
for(int i=0;i<f->n;i++)
{
 f->fr[i]=input();
}
}
int gcd_1(int n1,int d1)
{
int gcd=1;
for(int i=2;i<=n1/2 &&i<=d1/2;i++)
{
if(n1%i==0 && d1%i==0)
{
 gcd=i;
}
}
return gcd;
}


fract add(fract a, fract b)
{
//1/2  1/6
fract sum;
int gcd;
sum.num=((a.num*b.den)+(b.num*a.den));//8
sum.den=a.den*b.den;//12
gcd=gcd_1(sum.num,sum.den);//2
sum.num/=gcd;//4
sum.den/=gcd;//6
return sum;

}
void compute_n(frc *f )
{ 
    f->sum.num=f->fr[0].num;
    f->sum.den=f->fr[0].den;
    for(int i=1;i<f->n;i++)
    {
     f->sum=add(f->sum,f->fr[i]);//(a+b)+c
    }
	
}

void output_n(frc f)
{
for(int i=0;i<f.n;i++)
{
printf("the fraction %d/%d \n",f.fr[i].num,f.fr[i].den);
}
printf("the sum of  the fractions is  \n  %d/%d \n ",f.sum.num,f.sum.den);
}

int main()
{
int n;
frc f;
printf("enter number of fractions to be summed \n");
scanf("%d",&f.n);

input_n(&f);
compute_n(&f);
output_n(f);
return 0;
}