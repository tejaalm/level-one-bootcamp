//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct
{
  int num, den;
}Fract;
 
Fract input()
{
  Fract retval;
  
  printf("Enter 1st numerator and denominator: \n");
  scanf("%d %d", &retval.num, &retval.den);
 return retval;
}
int gcd_1(int d1,int d2)
{
  int gcd=1;
   for(int i=2;i<=d1/2 && i<=d2/2 ;i++)
  {
      if(d1%i==0&&d2%i==0)
      {
          gcd = i;       
      }
  }
  return gcd;
}
 
Fract add(Fract a, Fract b)
{
  Fract c;
  int gcd;
  c.num=((a.num*b.den)+(b.num*a.den));
  c.den=a.den*b.den;
  gcd=gcd_1(c.num,c.den);
  c.num=c.num/gcd;
  c.den=c.den/gcd;
  return c;
}

void output(Fract a, Fract b,Fract c)
{
   printf("The sum of %d/%d and %d/%d is %d/%d \n",a.num,a.den,b.num,b.den,c.num,c.den);
}
int main()
{
 Fract x,y, z;
  x= input();
  y=input();
  z=add(x,y);
  output(x,y,z);
  return 0;
 
}